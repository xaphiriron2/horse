{-# LANGUAGE PackageImports #-}
{-# LANGUAGE ExistentialQuantification #-}

module Types where

import Prelude hiding ((.), id)

import "GPipe-GLFW4" Graphics.GPipe.Context.GLFW (ModifierKeys(..), MouseButtonState(..), MouseButton(..), KeyState(..), Key(..))
import qualified "GPipe-GLFW4" Graphics.GPipe.Context.GLFW as GLFW
import Graphics.GPipe hiding (FlatVFloat(..), (^*), distance, Buffer, writeBuffer)
import qualified Graphics.GPipe as GP (Format(..), Buffer, writeBuffer)
import Codec.Picture

import Control.Category
import Control.Concurrent.STM (STM, atomically)
import Control.Concurrent.STM.TVar
import Control.Concurrent.STM.TChan
import Control.Monad.IO.Class
import Control.Monad.Exception

import Control.Monad
import Control.Monad.Writer
import Data.Bits
import Data.Char (ord)
import Data.Word
import Data.Vector.Storable (Vector, (!))
import qualified Data.Vector.Storable as V
import Data.Maybe
import Data.List
import Data.Foldable
import Data.Functor.Contravariant
import Foreign.Storable

import Linear

data EventHandler_ m e
	= forall s a. EHS
		{ zeroS :: s
		, generateS :: s -> e -> Writer [a] ()
		, updateS :: a -> s -> s
		, execute :: a -> m ()
		, activate :: m ()
		, deactivate :: m ()
		}

data EventHandlerRaw m s a e
	= EHSR
		{ zeroSR :: s
		, generateSR :: s -> e -> Writer [a] ()
		, updateSR :: a -> s -> s
		, executeR :: a -> m ()
		, activateR :: m ()
		, deactivateR :: m ()
		}

{- can't write this since `s` is on both sides of arrows
mergeHandlers :: (s -> s' -> t) -> (a -> a' -> b) -> EventHandlerRaw m s a e -> EventHandlerRaw m s' a' e -> EventHandlerRaw m t b e
mergeHandlers fs fa (EHSR z g u e act deact) (EHSR z' g' u' e' act' deact') = EHSR
	(fs z z')
	(\s e)
-}

statelessHandler :: (e -> Writer [a] ()) -> (a -> m ()) -> m () -> m () -> EventHandler_ m e
statelessHandler g e act deact  = EHS () (const g) (const id) e act deact

data Those a b = This a | That b | Those a b
	deriving (Eq, Ord, Show, Read)

-- note that this ends up ordering a and b relative to each other: everything `a` emits now happens before anything `b` emits. that's not great. (& it might be good to go full reactive-banana and give things logical times so this can be merged cleanly.) that being said we're only ever running these one-at-a-time anyway, so if they both emit the `as` would be in front of the `bs` on a simultanious occurance _anyway_ due to the `>>` sequencing in the merged `execute`
those :: Writer [a] () -> Writer [b] () -> Writer [Those a b] ()
those a b = tell (as <> bs)
	where
		as = This <$> execWriter a
		bs = That <$> execWriter b

instance Contravariant (EventHandler_ m) where
	contramap f (EHS z g u e act deact) = EHS z (\s -> g s . f) u e act deact

instance Monad m => Semigroup (EventHandler_ m e) where
	(EHS z g u e act deact) <> (EHS z' g' u' e' act' deact') = EHS
		(z, z')
		(\(as, bs) e -> those (g as e) (g' bs e))
		(\ab (as, bs) -> case ab of
			This a -> (u a as, bs)
			That b -> (as, u' b bs)
			Those a b -> (u a as, u' b bs)
		)
		(\ab -> case ab of
			This a -> e a
			That b -> e' b
			Those a b -> e a >> e' b
		)
		(act >> act')
		(deact >> deact')

instance Monad m => Monoid (EventHandler_ m e) where
	mempty = EHS
		()
		(\_ _ -> nothing)
		(const id)
		(const $ pure ())
		(pure ())
		(pure ())


type EventHandler os = EventHandler_ (ContextT GLFW.Handle os IO) GLFWEvent

-- type Moment a = Writer [a] () -- if i want to continue pretending i have a real event handler :V

nothing :: Monoid a => Writer a ()
nothing = pure ()

emit :: a -> Writer [a] ()
emit a = tell . pure $ a

emitFromMaybe :: Maybe a -> Writer [a] ()
emitFromMaybe Nothing = nothing
emitFromMaybe (Just a) = emit a



data ExecutionState os = Execution
	{ _shouldQuit :: Bool
	, handlerChanged :: Bool
	, handlers :: [EventHandler os]
	, resolution :: V2 Int
	}

getHandler :: TVar (ExecutionState os) -> IO (EventHandler os)
getHandler t = (fromMaybe mempty . listToMaybe . handlers) <$> readTVarIO t
getQuit :: TVar (ExecutionState os) -> IO Bool
getQuit t = _shouldQuit <$> readTVarIO t

pushHandler :: TVar (ExecutionState os) -> EventHandler os -> GLFWContext os ()
pushHandler t h = do
	liftIO $ atomically $ modifyTVar' t $ \(Execution q _ hs res) -> Execution q True (h : hs) res
	activate h

-- | pops the current handler. returns false if there's no handler to pop
popHandler :: TVar (ExecutionState os) -> GLFWContext os Bool
popHandler t = do
	mhandler <- liftIO $ atomically $ stateTVar t $ \(Execution q _ hs res) -> case hs of
		[] -> (Nothing, Execution q True [] res)
		h:hs -> (Just $ (h, listToMaybe hs), Execution q True hs res)
	case mhandler of
		Nothing -> return False
		Just (poppedHandler, mactiveHandler) -> do
			deactivate poppedHandler
			fromMaybe (return ()) $ activate <$> mactiveHandler
			return True

clearHandlers :: TVar (ExecutionState os) -> GLFWContext os ()
clearHandlers t = do
	handlers <- liftIO $ atomically $ stateTVar t $ \(Execution q _ handlers res) -> (handlers, Execution q True [] res)
	deactivate `mapM_` handlers
	return ()

setQuit :: TVar (ExecutionState os) -> STM ()
setQuit t = modifyTVar' t $ \(Execution _ _ _ res ) -> Execution True True [] res

-- note how mouse button events do not come with position attached
data RawGLFWEvent
	= KeyEvent Key Int KeyState ModifierKeys -- idk what the Int is
	| CharEvent Char -- char read; probably not unicode safe
	| CursorPosEvent Double Double -- x y
	| CursorEnterEvent
	| CursorLeaveEvent
	| MouseButtonEvent MouseButton MouseButtonState ModifierKeys
	| ScrollEvent Double Double -- i have no clue what these values are
	| WindowCloseEvent
	deriving (Eq, Ord, Show, Read)

data GLFWEvent
	= MouseDown MouseButton (V2 Int)
	| MouseUp MouseButton (V2 Int)
	| MouseMove (V2 Int)
	-- | MouseDrag MouseButton (V2 Int) (V2 Int) -- fire this _additionally_ with mousemove when the cursor is moved while a mouse button is held down
	| Typed Char -- char read; probably not unicode safe
	| KeyDown Key
	| KeyUp Key
	| KeyRepeat Key
	| WindowClose
	deriving (Eq, Ord, Show, Read)

type GLFWContext os a = ContextT GLFW.Handle os IO a
