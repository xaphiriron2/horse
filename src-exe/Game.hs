{-# LANGUAGE PackageImports #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GADTs #-}

module Game where

import Prelude hiding ((.), id)

import "GPipe-GLFW4" Graphics.GPipe.Context.GLFW (ModifierKeys(..), MouseButtonState(..), MouseButton(..), KeyState(..), Key(..))
import qualified "GPipe-GLFW4" Graphics.GPipe.Context.GLFW as GLFW
import Graphics.GPipe hiding (FlatVFloat(..), (^*), distance, Buffer, writeBuffer)
import qualified Graphics.GPipe as GP (Format(..), Buffer, writeBuffer)
import Codec.Picture

import Control.Category
import Control.Concurrent.STM (STM, atomically)
import Control.Concurrent.STM.TVar
import Control.Concurrent.STM.TChan
import Control.Monad.IO.Class
import Control.Monad.Exception

import Control.Monad
import Control.Monad.Writer
import Data.Bits
import Data.Char (ord)
import Data.Word
import Data.Vector.Storable (Vector, (!))
import qualified Data.Vector.Storable as V
import Data.Maybe
import Data.Map (Map)
import qualified Data.Map as Map (insert, delete)
import Data.List
import Data.Foldable
import Data.Functor.Contravariant
import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal

import Linear

import Types


setupMainLoopCallbacks :: Window os RGBAFloat Depth -> TVar (ExecutionState os) -> GLFWContext os
	( EventHandler os -- input
	, Float -> GLFWContext os () -- physics
	, Float -> GLFWContext os () -- render
	)
setupMainLoopCallbacks window exec = do
	startingWindowSize <- liftIO $ resolution <$> readTVarIO exec

	-- shader setup
	-- these are both :: GP.Buffer os (Uniform (V4 (B4 Float)))
	cameraBuffer <- newBuffer 1
	modelviewBuffer <- newBuffer 1
	GP.writeBuffer cameraBuffer 0 . pure $ identity
	GP.writeBuffer modelviewBuffer 0 . pure $ identity
	shader <- basicShader window startingWindowSize cameraBuffer modelviewBuffer

	-- 'asset loading'
	fontsheet <- spritesheet "img/font.png" 8 8
	sprites <- spritesheet "img/sprites.png" 16 16

	-- the main wrinkle here is that `RenderObject`s aren't editable, since their various types are hidden in the existentials. the physics code would need to keep track of the `RenderObjectRaw` values in something more parametric so that it could update them, which might make the physics render update code a little messy
	renderCache <- liftIO $ newTVarIO $ (mempty :: Map Int (RenderObject os))
	let renderFunc = do
		cache <- liftIO $ readTVarIO renderCache
		renderObjectCache cache

	let makeNewGameHandler = do
		gameStateTVar <- liftIO $ newTVarIO (Nothing :: Maybe GameState)
		pcInputChannel <- liftIO $ newTChanIO

		let pcInputHandler = userInterfaceHandler startingWindowSize pcInputChannel
		-- this does nothing but write to the game state tvar when the handler is activated
		let newGameSetupHandler = EHS
			()
			(const . const nothing)
			(const id)
			(const $ pure ())
			(do
				let starterRoomId = 20

				let rStart = V2 2 2
				let rect = Rect rStart (V2 28 28)
				gameMap <- liftIO $ initializeMap (V2 32 32) $ \pos ->
					if inRect rect pos
						then case pos - rStart of
							V2 x y
								| x <= 3 && y <= 3 -> ((2,2,2,2), 255, ([],[],[],[]))
								| x <= 3 && y == 4 -> ((2,2,1,1), 255, ([],[],[],[]))
								| between  5  6 x && between 2 3 y -> ((2,2,2,2), 255, ([],[],[],[]))
								| between  7  8 x && between 2 3 y -> ((3,3,3,3), 255, ([],[],[],[]))
								| between 11 12 x && between 2 3 y -> ((3,3,3,3), 255, ([],[],[],[]))

							V2  3  9 -> ((1,1,2,1), 255, ([],[],[],[]))
							V2  3 10 -> ((1,2,2,1), 255, ([],[],[],[]))
							V2  3 11 -> ((1,2,1,1), 255, ([],[],[],[]))
							V2  4  9 -> ((1,1,2,2), 255, ([],[],[],[]))
							V2  4 10 -> ((2,2,2,2), 255, ([],[],[],[]))
							V2  4 11 -> ((2,2,1,1), 255, ([],[],[],[]))
							V2  5  9 -> ((1,1,1,2), 255, ([],[],[],[]))
							V2  5 10 -> ((2,1,1,2), 255, ([],[],[],[]))
							V2  5 11 -> ((2,1,1,1), 255, ([],[],[],[]))

							_ -> ((1,1,1,1), 255, ([],[],[],[]))
						else ((6,6,6,6), 255, ([],[],[],[]))
				worldBuffer <- renderMap gameMap


				let pcId = 65535
				let pcMatrix = do
					mgame <- readTVarIO gameStateTVar
					return $ pure $ case mgame of
						Nothing -> identity
						Just game ->
							let (ActorState pos _ _ yaw _) = pcState game
							in posAsMatrix pos yaw
				pcBuffer <- pcModel
				let pcObject = RenderObject $ RenderObjectRaw
					shader
					(sheetTexture sprites)
					TriangleList
					(Just $ AutoIO pcMatrix (UOne (Buffer modelviewBuffer 1) 0))
					[(pcBuffer, (0, bufferSize pcBuffer - 1))]

				let defaultCamera = Camera (V3 0 0 0) (V2 0 0) 90 120
				let pcStartingState = ActorState (V3 (5 * 16) 0 (5 * 16)) (V3 0 0 0) (V3 0 0 0) 0 Standing

				-- render indices 1-65535 will be for level geometry, w/ the camera matrix set to the game camera...
				liftIO $ atomically $ modifyTVar renderCache
					$ (addRenderObject 0
						$ CachedAction $ do
							mgame <- liftIO $ readTVarIO gameStateTVar
							let matrix = fromMaybe identity
								$ (\game -> cameraMatrix startingWindowSize
										(location . pcState $ game)
										(camera game))
									<$> mgame
							GP.writeBuffer cameraBuffer 0 [matrix]
							)
					-- and render indices 65537+ will be for ui elements, w/ the camera matrix set to identity
					. (addRenderObject 65536
						$ CachedAction $ GP.writeBuffer cameraBuffer 0 . pure $ identity)
					. (addRenderObject starterRoomId
						$ RenderObject $ RenderObjectRaw
							shader
							(sheetTexture sprites)
							TriangleList
							(Just $ AutoPure [identity] (UOne (Buffer modelviewBuffer 1) 0))
							[(worldBuffer, (0, bufferSize worldBuffer - 1))
							])
					. (addRenderObject pcId pcObject)

				-- write an initial game state
				liftIO $ atomically $ writeTVar gameStateTVar $ Just $ GameState
					defaultCamera
					starterRoomId
					(V2 0 0)
					Nothing
					pcStartingState
					gameMap
				-- clearHandlers exec >> pushHandler exec pcInputHandler -- idk if resetting handlers in an activation handler would work right; check that before uncommenting this
				)
			(return ())
		return (newGameSetupHandler <> pcInputHandler <> catchWindowClose exec, gameStateTVar, pcInputChannel)

	(newGameHandler, sharedGameStateVar, pcInputChannel) <- makeNewGameHandler
	startingHandler <- titleMenu exec fontsheet renderCache 1 shader
		$ Menu
			[("new game", clearHandlers exec >> pushHandler exec newGameHandler)
			,("options", pushHandler exec $ optionsPage exec)
			,("credits", pushHandler exec $ creditsPage exec)
			,("quit", pushHandler exec $ quitHandler exec)
			]

	let timestep = 0.016 -- or w/e, this should probably be a param somewhere

	let actualPhysicsTimestep = do
		gameState <- liftIO $ readTVarIO sharedGameStateVar
		pcInputEvents <- liftIO $ atomically $ collectTChan pcInputChannel
		case gameState of
			Nothing -> return ()
			Just state -> do
				let newImpel = sum $ extractImpel <$> pcInputEvents

				let cameraAction = case extractCameraControl `mapMaybe` pcInputEvents of
					[] -> CameraNone
					vs -> case maximum vs of
						CameraRotate r -> CameraRotate $ r ^* (timestep * 2)
						x -> x

				-- todo: write some lenses for this
				let state' = state
					{ pcImpulse = pcImpulse state + newImpel
					, cameraRotation = case cameraAction of
						CameraStop -> Nothing
						CameraStart -> Just (V2 0 0)
						CameraRotate r -> r <$ cameraRotation state
						_ -> cameraRotation state
					, camera = case cameraAction of
						_ -> (camera state) { rotation = rotation (camera state) + fromMaybe 0 (cameraRotation state) }
					}

				let to3d (V2 x z) = V3 x 0 z
				let pcMoveAngle = translationByCameraAngle (camera state) $ pcImpulse state'
				let angle (V2 x y) = (atan2 y x) * (-1)
				let oldLocation = location . pcState $ state'
				let attemptedMove = oldLocation + to3d (pcMoveAngle ^* (timestep * 60))

				let overTile =
					let V3 x _ z = attemptedMove
					in V2 (floor $ x / 16) (floor $ z / 16)

				{-
				the full 'physics' 'integration' would look like this:

				if pc just jumped -> change state to add acceleration 'up' force in current move direction & set state to falling
				if pc is falling -> damp upwards acceleration + integrate velocity (up to cap?) into position
					9.8 m/s^2

				generate movement ray (from both 'physics' (there are none currently) and impelled forces)
				calculate out all tiles within 8 units of any point on the line (usually just 4 or 6 tiles)
				detect w/ all of them, checking height vs. pc height and finding collision points
				return the first collision where the tile height is > pc height (minus some small slope constant), if any

				no collision -> allow move
				collision -> stop movement (& maybe rebound or slide to produce a _new_ movement ray &, i guess, repeat)

				if pc is falling and their movement ray would move them under the height of the tile, set height to floor height and turn off falling
				if the pc _isn't_ falling, set their height to the tile height of their new location

				-}

				-- getTile :: GameMap -> V2 Int -> Maybe (IO TileData)
				actualNewPos <- liftIO $ case getTile (loadedMap state) overTile of
					Nothing -> pure oldLocation -- don't move out of bounds
					Just ioTile -> do
						(corners, _, _) <- ioTile
						let V3 rx _ rz = attemptedMove
						let y = heightOnTile corners (V2 ((rx `mod''` 16) / 16) ((rz `mod''` 16) / 16))
						return $ V3 rx (y * 16) rz

				let state'' = state'
					{ pcState = (pcState state')
						{ location = actualNewPos
						, yaw = if pcMoveAngle /= 0 then angle pcMoveAngle else yaw (pcState state')
						}
					}

				-- todo: figure out how to effectively update render cache / load and unload game zones
				liftIO $ atomically $ writeTVar sharedGameStateVar $ Just state''
				return ()

	accum <- liftIO $ newTVarIO 0
	return
		( startingHandler
		, \elapsed -> do
			-- extract however many timesteps should be run
			steps <- liftIO $ atomically $ stateTVar accum
				$ \acc ->
					( floor $ (acc + elapsed) / timestep -- number of steps to run, extracted
					, (acc + elapsed) `mod''` timestep -- remainder of accumulator, new accumulator value
					)
			-- run `physics` `steps` number of times
			sequence_ $ take steps $ repeat actualPhysicsTimestep
			return ()
		, \elapsed -> do
			render $ do
				clearWindowColor window (V4 0 0 0 1 :: Fractional b => V4 b)
				clearWindowDepth window 1
			renderFunc
			swapWindowBuffers window
		)


translate :: Num a => V3 a -> M44 a
translate (V3 x y z) = V4
	(V4 1 0 0 x)
	(V4 0 1 0 y)
	(V4 0 0 1 z)
	(V4 0 0 0 1)

heading :: Floating a => a -> M44 a
heading t = V4
	(V4   c  0 s 0)
	(V4   0  1 0 0)
	(V4 (-s) 0 c 0)
	(V4   0  0 0 1)
	where
		c = cos t
		s = sin t

posAsMatrix :: V3 Float -> Float -> M44 Float
posAsMatrix pos yaw = translate pos !*! heading yaw


-- CHECK: does this reverse the events? maybe
collectTChan :: TChan a -> STM [a]
collectTChan ch =
	let go = do
		res <- tryReadTChan ch
		case res of
			Nothing -> return []
			Just r -> (r :) <$> go
	in go

defaultMenuLayout pos font = MenuLayout
	(Colors
		(V3 1 1 1) -- text color
		(V3 (170/255) (170/255) (170/255)) -- disabled
		(V3 (170/255) (255/255) (170/255)) -- hover
		(V3 (255/255) (170/255) ( 85/255)) -- label
	)
	pos
	(Centered 0)
	font
	(SpecialChars
		9 -- cursorIx
		11 -- checkbox, empty
		12 -- checkbox, checked
		14 -- radio, unselected
		15 -- radio, selected
	)
	2 -- padding (in pixels, pre-zoom)
	2 -- zoom (multiplier)

titleMenu :: Ord k => TVar (ExecutionState os) -> Spritesheet os -> TVar (RenderCache k os) -> k -> BasicShader os -> Menu os -> GLFWContext os (EventHandler os)
titleMenu exec font renderCache renderIx shader menu = do
	let itemLength = (+ 2) . sum $ length . fst <$> menuItems menu
	buffer <- setupBuffer (itemLength * 6) []
	V2 screenWidth screenHeight <- liftIO $ resolution <$> readTVarIO exec
	return $ menuHandler exec menu renderCache renderIx shader
		(defaultMenuLayout (V2 (screenWidth `div` 2) ((screenHeight `div` 3) * 2)) font)
		buffer
		<> catchWindowClose exec

creditsPage tvar = textLayoutHandler tvar
	["made by xax c. july 2021-????"
	,"rendering with GPipe and GLFW"
	,"[[<<back|__pop]]"
	]
	<> catchWindowClose tvar
optionsPage tvar = textLayoutHandler tvar
	["options page"
	]
	<> catchWindowClose tvar

{-
text parser stuff:

[[foo|bar]] -- text displays `foo` as link, click link to go to `bar` (where that's a lookup on some big, like, `Map String (EventHandler os)` object that's maybe stored in the execution state
[[foo]] -- text displays `foo` as link, click link to go to `foo`
[[foo:bar]] -- text displays `foo` as link/hover, hover over link to display tooltip containing the formatted text from passage `bar`, which would maybe be another big lookup map somewhere

`__pop` would be a special-case passage title that pops the current handler

the goal would be to have a lot of scenes in the game be just twine-formatted plaintext in various files
-}

{-
TODO: run parsec on the input string to generate formatted text, & then render that appropriately, w/ relevant hover handlers for tooltip & hover/click actions, etc.
-}
textLayoutHandler :: TVar (ExecutionState os) -> [String] -> EventHandler os
textLayoutHandler tvar text = statelessHandler
	(catchRelease)
	(\() -> do
		liftIO $ putStrLn $ "leaving text layout..."
		popHandler tvar
		return ()
	)
	(liftIO $ do
		sequence_ $ putStr <$> text
	)
	(return ())
	where
		catchRelease :: GLFWEvent -> Writer [()] ()
		catchRelease ev = case ev of
			MouseUp _ _ -> tell [()]
			_ -> pure ()

quitHandler :: TVar (ExecutionState os) -> EventHandler os
quitHandler exec = statelessHandler
	(const nothing)
	(const $ pure ())
	(liftIO $ do
		putStrLn "quitHandler init"
		atomically $ setQuit exec)
	(return ())

catchWindowClose :: TVar (ExecutionState os) -> EventHandler os
catchWindowClose exec = statelessHandler
	(catchClose)
	(\() -> do
		let quit = quitHandler exec
		pushHandler exec $ quit
		)
	(liftIO $ putStrLn "catchWindowClose init" >> pure ())
	(return ())
	where
		catchClose :: GLFWEvent -> Writer [()] ()
		catchClose ev = case ev of
			WindowClose -> emit ()
			_ -> nothing

filterEE :: (a -> Bool) -> Maybe a -> Maybe a
filterEE f Nothing = Nothing
filterEE f (Just v)
	| f v = Just v
	| otherwise = Nothing

data Alignment = LeftAligned | RightAligned | Centered Int
	deriving (Eq, Ord, Show, Read)

data Menu os = Menu
	{ menuItems :: [(String, GLFWContext os ())] -- would maybe need something to show 'disabled'
	}

data MenuEvents
	= HoverIn Int
	| HoverOut Int
	| Click Int
	deriving (Eq, Ord, Show, Read)

data MenuLayout os = MenuLayout
	{ colors :: Colors
	, position :: V2 Int
	, align :: Alignment
	, font :: Spritesheet os
	, fontchars :: SpecialChars
	, linePadding :: Int
	, zoom :: Int
	}
data Colors = Colors
	{ text :: V3 Float -- general text color
	, disabled :: V3 Float -- text color for disabled menu items, form fields, or links
	, hover :: V3 Float -- text color for activatable items when hovered over
	, label :: V3 Float -- text color for form labels
	}
data SpecialChars = SpecialChars
	{ cursor :: Int
	, checkboxEmpty :: Int
	, checkboxChecked :: Int
	, radioEmpty :: Int
	, radioSelected :: Int
	}


-- this currently has some poor behavior if you use keys to select an item and then wiggle the mouse around outside of the menu -- it'll 'deselect' the menu item even though the index will still be there. one way to solve this would be to stop letting the mouse deselect anything, unless it's also selecting something else.
menuHandler :: forall os k. Ord k => TVar (ExecutionState os) -> Menu os -> TVar (RenderCache k os) -> k -> BasicShader os -> MenuLayout os -> Buffer os BasicVertex -> EventHandler os
menuHandler exec (Menu items) renderCache renderIx shader layout buffer = EHS Nothing
	(\lastHover ev -> let
			scrollDown hover = case hover of
				Nothing -> emit $ HoverIn 0
				Just n | n+1 == length items -> nothing
					| otherwise -> emit (HoverOut n) >> emit (HoverIn (n+1))
			scrollUp hover = case hover of
				Nothing -> emit $ HoverIn 0
				Just n | n == 0 -> nothing
					| otherwise -> emit (HoverOut n) >> emit (HoverIn (n-1))
		in case ev of
			-- maybe hoverin/hoverout
			MouseMove px -> case listToMaybe . filter (\(_, r) -> inRect r px) $ zip [0..] hitRects of
				Nothing -> case lastHover of
					Nothing -> pure ()
					Just i -> emit $ HoverOut i
				Just (i, _) -> case lastHover of
					Nothing -> emit $ HoverIn i
					Just j | i == j -> pure () -- still in the same rect
						-- leaving old hover, entering new hover
						| otherwise -> emit (HoverOut j) >> emit (HoverIn i)
			-- keyboard events (& hopefully joystick events eventually) just scroll down/up
			KeyDown Key'Down -> scrollDown lastHover
			KeyDown Key'Up -> scrollUp lastHover
			KeyRepeat Key'Down -> scrollDown lastHover
			KeyRepeat Key'Up -> scrollUp lastHover
			-- todo: enter/space would also send a click event
			-- maybe click
			MouseDown b px | b /= MouseButton'1 -> pure () -- only care about left-clicks here
				| otherwise -> case listToMaybe . filter (\(_, r) -> inRect r px) $ zip [0..] hitRects of
					Nothing -> pure ()
					Just (i, _) -> emit $ Click i
			_ -> pure ()
	)
	-- which item is currently selected, if any
	(\ev lastHover -> case ev of
		HoverIn n -> Just n
		HoverOut n | Just n == lastHover -> Nothing
		_ -> lastHover
	)
	(\ev -> let
			lzoom = zoom (layout :: MenuLayout os)
		in case ev of
			HoverIn n -> do
				screensize <- liftIO $ resolution <$> readTVarIO exec
				let Rect pos _ = hitRects !! n
				let string = fst $ items !! n
				let cursorOffset = V2 ((x_ . spriteSize . font $ layout) * lzoom) 0
				-- rewrite n as hover color
				writeUIVerts buffer 0 (text . colors $ layout)
					$ prepareSymbolsUI screensize (font layout) (pos - cursorOffset) lzoom [cursor . fontchars $ layout]
				writeUIVerts buffer (vOffsets !! n) (hover . colors $ layout)
					$ prepareStringUI screensize (font layout) pos lzoom string
				return ()
			HoverOut n -> do
				screensize <- liftIO $ resolution <$> readTVarIO exec
				let Rect pos _ = hitRects !! n
				let string = fst $ items !! n

				writeUIVerts buffer (vOffsets !! n) (text . colors $ layout)
					$ prepareStringUI screensize (font layout) pos lzoom string
				return ()
			Click n -> do
				-- run the given item's handler
				let selected = snd $ items !! n
				selected
	)
	(do
		let posTexts = zipWith (\(Rect offset _) text -> (offset, text)) hitRects $ fst <$> items
		let lzoom = zoom (layout :: MenuLayout os)
		screensize <- liftIO $ resolution <$> readTVarIO exec

		sequence_ $ snd $ mapAccumL (\o (pos, string) ->
			( o + length string * 6 -- each character turns into 6 vertices
			, writeUIVerts buffer o (text . colors $ layout)
				$ prepareStringUI screensize (font layout) pos lzoom string
			)) 6 posTexts

		liftIO $ atomically $ modifyTVar renderCache $ addRenderObject renderIx
			$ RenderObject $ RenderObjectRaw
				shader
				(sheetTexture . font $ layout)
				TriangleList
				Nothing
				[(buffer, (0, bufferSize buffer - 1))
				]
	)
	-- i don't _think_ we need to explicitly free the buffer; haskell garbage collection should take care of the allocation, assuming gpipe has registered the foreignptr callbacks
	(do
		liftIO $ atomically $ modifyTVar renderCache $ removeRenderObject renderIx
		return ())
	where
		start = position (layout :: MenuLayout os)
		yPadding = linePadding layout + height
		height = spriteHeight . font $ layout
		lzoom = zoom (layout :: MenuLayout os)
		vOffsets = scanl' (+) 6 $ (\str -> length str * 6) . fst <$> items
		hitRects = zipWith (\i str ->
			let
				textWidth = stringLength (font layout) str * lzoom
				lzoom = zoom (layout :: MenuLayout os)
				textHeight = height * lzoom
				xOffset = case align layout of
					LeftAligned -> V2 0 0
					RightAligned -> V2 (-textWidth) 0
					Centered width -> V2 ((width - textWidth) `div` 2) 0
				yOffset = V2 0 (yPadding * i * lzoom)
			in Rect
					(start + xOffset + yOffset)
					(V2 textWidth textHeight)
				) [0..] (fst <$> items)


type CameraUniform os = GP.Buffer os (Uniform (V4 (B4 Float)))

viewport :: V2 Int -> ViewPort
viewport dim = ViewPort (V2 0 0) dim

x_ :: V2 a -> a
x_ (V2 x _) = x

y_ :: V2 a -> a
y_ (V2 _ y) = y

w :: V3 a -> a -> V4 a
w (V3 x y z) _w = V4 x y z _w

z_ :: V4 a -> a
z_ (V4 _ _ z _) = z

xyz_ :: V4 a -> V3 a
xyz_ (V4 x y z _) = V3 x y z


type RenderCache k os = Map k (RenderObject os)

renderObjectCache :: Ord k => RenderCache k os -> GLFWContext os ()
renderObjectCache cache = renderObject `mapM_` cache

addRenderObject :: Ord k => k -> RenderObject os -> RenderCache k os -> RenderCache k os
addRenderObject key val map = Map.insert key val map

removeRenderObject :: Ord k => k -> RenderCache k os -> RenderCache k os
removeRenderObject key map = Map.delete key map

data UniformWrite os a where
	UNil :: UniformWrite os ()
	UOne :: Buffer os (Uniform a) -> Int -> UniformWrite os a
	UPair :: Buffer os (Uniform a) -> Int -> UniformWrite os b -> UniformWrite os (a, b)

applyUniform :: [HostFormat a] -> UniformWrite os a -> GLFWContext os ()
applyUniform vs u = case u of
	UNil -> pure ()
	UOne buf i -> writeBuffer buf i vs
	UPair buf i rest -> let (has, ras) = unzip vs
		in writeBuffer buf i has >> applyUniform ras rest

data AutoUniform os a where
	AutoPure :: [HostFormat a] -> UniformWrite os a -> AutoUniform os a
	AutoIO :: IO [HostFormat a] -> UniformWrite os a -> AutoUniform os a

autoWrite :: AutoUniform os a -> GLFWContext os ()
autoWrite a = case a of
	AutoPure vs u -> applyUniform vs u
	AutoIO iovs u -> do
		vs <- liftIO iovs
		applyUniform vs u

data RenderObject os
	= forall t v s. PrimitiveTopology t => RenderObject (RenderObjectRaw os t v s)
	-- this exists so we can put non-rendering gpipe actions into the render cache, to be executed during rendering. this is for e.g., uniform writes
	| CachedAction (GLFWContext os ())

data RenderObjectRaw os t v s = forall a. RenderObjectRaw
	{ shader :: CompiledShader os (PrimitiveArray t v, s)
	, statics :: s
	, topology :: t
	, autoUniforms :: Maybe (AutoUniform os a)
	-- for more advanced buffers there could be functions like `b -> [(Int, Int)]` so you could determine the render order at runtime, like e.g., camera position for a painter's algorithm tree. but painter's algorithm is way less efficient than z-buffers and there's no translucent objects yet so it doesn't really matter
	, buffers :: [(Buffer os v, (Int, Int))]
	}

renderObject :: RenderObject os -> GLFWContext os ()
renderObject r = case r of
	RenderObject raw -> renderObjectRaw raw
	CachedAction act -> act

-- this could return `Either (Render os ()) (GLFWContext os ())` based on the existance of a `uniforms` & that would maybe make some rendering 'faster' (since it would avoid the `render` call and let things be merged directly in the render monad) but idk if that's worth the added type complexity
renderObjectRaw :: PrimitiveTopology t => RenderObjectRaw os t v s -> GLFWContext os ()
renderObjectRaw (RenderObjectRaw shader statics topology uniforms buffers) = do
	case uniforms of
		Nothing -> pure ()
		Just u -> autoWrite u
	render $ (\(buff, bounds) -> applyShader shader topology buff bounds statics) `mapM_` buffers

data Buffer os a = Buffer
	{ gpipeBuffer :: GP.Buffer os a
	, bufferSize  :: Int
	}
	deriving (Eq)

-- these are more a reminder of what goes into actually rendering a thing than useful rendering functions
-- allocate a new buffer, potentially with some initial values. buffers don't need to be explicitly freed; gpipe registers a finalizer that frees the texture name
setupBuffer :: BufferFormat v => Int -> [HostFormat v] -> GLFWContext os (Buffer os v)
setupBuffer size initial = do
	buffer <- newBuffer size
	GP.writeBuffer buffer 0 (take size initial)
	return $ Buffer buffer size

writeBuffer :: Buffer os v -> Int -> [HostFormat v] -> GLFWContext os ()
writeBuffer (Buffer buffer size) offset newData =
	GP.writeBuffer buffer offset (take (size - offset) newData)

-- apply a shader to a slice of a buffer's vertices, producing a render object
applyShader :: PrimitiveTopology t => CompiledShader os (PrimitiveArray t v, s) -> t -> Buffer os v -> (Int, Int) -> s -> Render os ()
applyShader shader t buffer (lo, hi) s = do
	array <- newVertexArray $ gpipeBuffer buffer
	let vertices = takeVertices hi $ dropVertices lo $ array
	shader (toPrimitiveArray t vertices, s)

-- `render` prepares a render object to be executed in a glfw context
renderBuffer :: PrimitiveTopology t => CompiledShader os (PrimitiveArray t v, s) -> t -> Buffer os v -> s -> GLFWContext os ()
renderBuffer shader t buffer s = render
	$ applyShader shader t buffer (0, bufferSize buffer - 1) s

type BasicVertex = (B3 Float, B3 Float, B3 Float, B2 Float)

type BasicShader os = CompiledShader os
	( PrimitiveArray Triangles BasicVertex
	, Texture2D os (GP.Format RGBAFloat)
	)

-- flat-color shader taking point, color, normal, and uv. normal isn't used at all since this does no lighting
-- `CompiledShader` is just a type over an `s -> Render os ()` function; when run this produces a function you can give vertex data (primitivearray + texture2d) to produce a render value
basicShader :: (ContextHandler ctx, MonadIO m, MonadException m) => Window os RGBAFloat Depth -> V2 Int -> CameraUniform os -> CameraUniform os ->
	ContextT ctx os m
		( CompiledShader os
			( PrimitiveArray Triangles BasicVertex
			, Texture2D os (GP.Format RGBAFloat)
			)
		)
basicShader window windowSize cameraBuffer modelviewBuffer = compileShader $ do

	camera <- getUniform $ \_ -> (cameraBuffer, 0)
	view <- getUniform $ \_ -> (modelviewBuffer, 0)

	samp <- newSampler2D (\(_, t) ->
		( t
		, SamplerFilter Nearest Nearest Nearest Nothing
		, (pure Repeat, V4 1 0 1 1)
		))
	let sampleTexture = sample2D samp SampleAuto Nothing Nothing

	primitiveStream <- toPrimitiveStream $ \(v, _) -> v
	let primitiveStream2 = (\(pt, color, norm, uv) ->
			( (camera !*! view) !* w pt 1 -- update pt based on camera + viewmatrix transforms
			, ( color
				, xyz_ $ view !* w norm 0 -- update the object's normals based on its position (but not the camera position)
				, uv
				)
			))
		<$> primitiveStream
	fragmentStream <- withRasterizedInfo
			(\(color, norm, uv) r ->
				( w color 1 * sampleTexture uv -- sample texture based on UV & blend with vertex color to produce final color
				, z_ $ rasterizedFragCoord r -- set depth to fragment depth
				)
			)
		<$> rasterize
			(const (Front, PolygonFill, viewport windowSize, DepthRange 0 1))
			primitiveStream2
	drawWindowColorDepth
		(\_ ->
			( window
			, ContextColorOption
				(BlendRgbAlpha
					(FuncAdd, FuncAdd)
					(BlendingFactors SrcAlpha OneMinusSrcAlpha, BlendingFactors One One)
					(V4 1 0 1 1)
				)
				(V4 True True True True)
			, DepthOption Less True
			)
		)
		fragmentStream

data LoadedImage os = LoadedImage
	{ image :: Texture2D os (Format RGBAFloat)
	, size :: V2 Int
	}

loadImage :: String -> GLFWContext os (Either String (LoadedImage os))
loadImage path = do
	rawTex <- liftIO $ readImage path
	let res = (\image -> case image of
		ImageY8 (Image w h px) ->
			Right ( V2 w h , a 1 . fmap ((/ 255) . fromIntegral) <$> spreadToV3 px )
		ImageY16 (Image w h px) ->
			Right ( V2 w h , a 1 . fmap ((/ 65535) . fromIntegral) <$> spreadToV3 px )
		ImageY32 (Image w h px) ->
			Right ( V2 w h , a 1 . fmap ((/ 4294967295) . fromIntegral) <$> spreadToV3 px )
		ImageYF (Image w h px) ->
			Right ( V2 w h , a 1 <$> spreadToV3 px )
		ImageYA8 (Image w h px) ->
			Right ( V2 w h , fmap ((/ 255) . fromIntegral) <$> spreadToV4 px )
		ImageYA16 (Image w h px) ->
			Right ( V2 w h , fmap ((/ 65535) . fromIntegral) <$> spreadToV4 px )
		ImageRGB8 (Image w h px) ->
			Right ( V2 w h , a 1 . fmap ((/ 255) . fromIntegral) <$> splitToV3 px )
		ImageRGB16 (Image w h px) ->
			Right ( V2 w h , a 1 . fmap ((/ 65535) . fromIntegral) <$> splitToV3 px )
		ImageRGBF (Image w h px) ->
			Right ( V2 w h , a 1 <$> splitToV3 px )
		ImageRGBA8 (Image w h px) ->
			Right ( V2 w h , fmap ((/ 255) . fromIntegral) <$> splitToV4 px )
		ImageRGBA16 (Image w h px) ->
			Right ( V2 w h , fmap ((/ 65535) . fromIntegral) <$> splitToV4 px )
		-- these could be handled, but i don't know how to translate them into RGB offhand
		ImageYCbCr8 _ -> Left $ "can't load image type @ "<> path <> " (YCbCr8 aka jpeg)"
		ImageCMYK8 _ -> Left $ "can't load image type @ "<> path <> " (CMYK8; are you trying to print a magazine?)"
		ImageCMYK16 _ -> Left $ "can't load image type @ "<> path <> " (CMYK16; are you trying to print a magazine?)"
		-- _ -> Left $ "general error loading image @ "<> path
		) =<< rawTex
	case res of
		Left err -> pure $ Left err
		Right (size, pxStream :: [V4 Float]) -> do
			tex <- newTexture2D GP.SRGB8A8 size 1
			-- this might produce backwards textures, since iirc opengl textures start from the bottom row and juicypixels would start from the top
			writeTexture2D tex 0 (V2 0 0) size pxStream
			return $ Right $ LoadedImage tex size
	where
		a :: a -> V3 a -> V4 a
		a _w (V3 x y z) = V4 x y z _w

spreadToV3 :: Storable a => Vector a -> [V3 a]
spreadToV3 v = (\i -> V3 i i i) <$> V.toList v

spreadToV4 :: Storable a => Vector a -> [V4 a]
spreadToV4 = go
	where
		toV4 :: Storable a => Vector a -> V4 a
		toV4 v = V4 (v ! 0) (v ! 0) (v ! 0) (v ! 1)
		go :: Storable a => Vector a -> [V4 a]
		go vs | V.null vs = []
			| otherwise = case V.splitAt 2 vs of
				(h, rs) -> toV4 h : go rs

splitToV3 :: Storable a => Vector a -> [V3 a]
splitToV3 = go
	where
		toV3 :: Storable a => Vector a -> V3 a
		toV3 v = V3 (v ! 0) (v ! 1) (v ! 2)
		go :: Storable a => Vector a -> [V3 a]
		go vs | V.null vs = []
			| otherwise = case V.splitAt 3 vs of
				(h, rs) -> toV3 h : go rs

splitToV4 :: Storable a => Vector a -> [V4 a]
splitToV4 = go
	where
		toV4 :: Storable a => Vector a -> V4 a
		toV4 v = V4 (v ! 0) (v ! 1) (v ! 2) (v ! 3)
		go :: Storable a => Vector a -> [V4 a]
		go vs | V.null vs = []
			| otherwise = case V.splitAt 4 vs of
				(h, rs) -> toV4 h : go rs


data Rect a = Rect
	{ rectOffset :: V2 a
	, rectSize :: V2 a
	}
	deriving (Eq, Ord, Show, Read)

rectFar :: Num a => Rect a -> V2 a
rectFar (Rect lo s) = lo + s

rectCorner :: Num a => Rect a -> Int -> V2 a
rectCorner (Rect o (V2 w h)) corner = case corner `mod` 4 of
	0 -> o
	1 -> o + V2 w 0
	2 -> o + V2 w h
	3 -> o + V2 0 h
	_ -> 0

inRect :: (Num a, Ord a) => Rect a -> V2 a -> Bool
inRect (Rect (V2 x y) (V2 w h)) (V2 px py) = between x (x+w) px && between y (y+h) py

between :: Ord a => a -> a -> a -> Bool
between lo hi x = lo <= x && x <= hi

data Spritesheet os = Spritesheet
	{ spriteImage :: LoadedImage os
	, spriteSize :: V2 Int
	-- , spriteOffset :: V2 Int
	}
spriteWidth :: Spritesheet os -> Int
spriteWidth (Spritesheet _ (V2 w _)) = w
spriteHeight :: Spritesheet os -> Int
spriteHeight (Spritesheet _ (V2 _ h)) = h
sheetTexture :: Spritesheet os -> Texture2D os (Format RGBAFloat)
sheetTexture sheet = image . spriteImage $ sheet

stringLength :: Spritesheet os -> String -> Int
stringLength sheet str =
	let spriteWidth = x_ . spriteSize $ sheet
	in spriteWidth * length str

spritesheet :: String -> Int -> Int -> GLFWContext os (Spritesheet os)
spritesheet path spriteWidth spriteHeight = do
	res <- loadImage path
	case res of
		Left err -> error err
		Right image -> return $ Spritesheet image (V2 spriteWidth spriteHeight)

spriteUVs :: Spritesheet os -> Int -> Rect Float
spriteUVs (Spritesheet (LoadedImage _ (V2 w h)) (V2 sw sh)) i =
		Rect (V2 left top) (V2 (xo 1) (yo 1))
	where
		left = xo x
		right = xo $ x + 1
		top = yo y
		bottom = yo $ y + 1
		x = i `mod` cols
		y = i `div` cols
		xo i = (fromIntegral sw / fromIntegral w) * fromIntegral i
		yo i = (fromIntegral sh / fromIntegral h) * fromIntegral i
		cols = w `div` sw
		rows = h `div` sh

-- resolution and position. returns coord placement given identity camera matrix
spritePos :: V2 Int -> V2 Int -> V2 Float
spritePos (V2 sw sh) (V2 xp yp) = V2 (x xp) (y yp)
	where
		x i = (fromIntegral i / fromIntegral sw) * 2 - 1
		y i = (fromIntegral (sh - i) / fromIntegral sh) * 2 - 1

-- returns the size of the point in screen-space coords
spriteOffset :: V2 Int -> V2 Int -> V2 Float
spriteOffset (V2 sw sh) (V2 xp yp) = V2 (x xp) (y yp)
	where
		x i = (fromIntegral i / fromIntegral sw) * 2
		y i = (fromIntegral i / fromIntegral sh) * (-2)

prepareStringUI :: V2 Int -> Spritesheet os -> V2 Int -> Int -> String
	-> [(Rect Float, Rect Float)]
prepareStringUI resolution spritesheet start zoom string = prepareSymbolsUI resolution spritesheet start zoom $ ord <$> string

prepareSymbolsUI :: V2 Int -> Spritesheet os -> V2 Int -> Int -> [Int]
	-> [(Rect Float, Rect Float)]
prepareSymbolsUI resolution spritesheet start zoom symbols = let
		width = spriteWidth spritesheet
		xOffset w = spriteOffset resolution (V2 w 0) ^* fromIntegral zoom
		yOffset h = spriteOffset resolution (V2 0 h) ^* fromIntegral zoom
		topLeft = spritePos resolution start

		uvs = spriteUVs spritesheet <$> symbols
		widths = (xOffset . const width) <$> symbols
		pos = posRect (spriteSize spritesheet) <$> scanl (+) topLeft widths
		posRect :: V2 Int -> V2 Float -> Rect Float
		posRect (V2 x y) topLeft = Rect topLeft (xOffset x + yOffset y)
		posUVs = zip pos uvs
		-- stitch together uvs + pos
	in posUVs

-- | returns the number of vertices written
writeUIVerts :: Buffer os BasicVertex -> Int -> V3 Float -> [(Rect Float, Rect Float)] -> GLFWContext os Int
writeUIVerts buf offset color symbols =
	let
		-- generate 6 verts to draw a quad for each symbol
		genVerts :: (Rect Float, Rect Float) -> [(V3 Float, V3 Float, V3 Float, V2 Float)]
		genVerts (pos, uv) =
			let
				norm = V3 0 0 1
				fullPos i = let V2 x y = rectCorner pos i
					in V3 x y (-1)

			in
				[ (fullPos 0, color, norm, rectCorner uv 0)
				, (fullPos 2, color, norm, rectCorner uv 2)
				, (fullPos 1, color, norm, rectCorner uv 1)

				, (fullPos 0, color, norm, rectCorner uv 0)
				, (fullPos 3, color, norm, rectCorner uv 3)
				, (fullPos 2, color, norm, rectCorner uv 2)
				]
		vertData = genVerts =<< symbols
	in do
		writeBuffer buf offset vertData
		return $ length vertData

-- the camera doesn't store what it's focused on; that needs to be provided. since they're generally attached to other objects that are moving around in other ways, it seemed redundant to have it store a position that would need to be continually updated anyway
data Camera a = Camera
	{ displace :: V3 a -- center displacement from focus (e.g., over-the-shoulder camera)
	, rotation :: V2 a -- yaw and pitch
	, fieldOfView :: a -- in degrees
	, depth :: a -- determines the near/far clipping planes & the default distance from target
	}
	deriving (Eq, Ord, Show, Read)

rotate2D :: Float -> V2 Float -> V2 Float
rotate2D t (V2 x y) = V2 (x * cos t - y * sin t) (y * cos t + x * sin t)

translationByCameraAngle :: Camera Float -> V2 Float -> V2 Float
translationByCameraAngle cam trans = rotate2D (-rx) $ normalize trans
	where
		(V2 rx _) = rotation cam

cameraMatrix :: (Floating a, Epsilon a) => V2 Int -> V3 a -> Camera a -> V4 (V4 a)
cameraMatrix resolution focus (Camera displace (V2 yaw pitch) fov_ depth) =
	perspective fov aspect zNear zFar !*!
	Linear.lookAt (V3 depth depth 0) (V3 0 0 0) (V3 0 1 0) !*!
	mkTransformation (axisAngle (V3 0   0  1) pitch) 0 !*!
	mkTransformation (axisAngle (V3 0 (-1) 0) yaw) 0 !*!
	translationMatrix (-focus + displace)
	where
		fov = deg2rad fov_
		aspect = width / height
		-- these depth constants are totally arbitrary values & probably could be fixed to something, but this works decently so far
		zNear = depth / 6
		zFar = depth * 500

		V2 width height = fromIntegral <$> resolution
		deg2rad d = d / 180 * pi
		translationMatrix (V3 x y z) = V4
			(V4 1 0 0 x)
			(V4 0 1 0 y)
			(V4 0 0 1 z)
			(V4 0 0 0 1)


data ControlEvent
	= CameraStartMovement
	| CameraStopMovement
	| CameraMotion (V2 Float)
	| PCStartImpel (V2 Float)
	| PCStopImpel (V2 Float)
	-- | PcAction LightAttack | HeavyAttack | Jump | etc
	deriving (Eq, Ord, Show, Read)

extractImpel :: ControlEvent -> V2 Float
extractImpel c = case c of
	PCStartImpel v -> v
	PCStopImpel v -> v ^* (-1)
	_ -> 0

extractCameraAngles :: ControlEvent -> V2 Float
extractCameraAngles c = case c of
	CameraMotion v -> v
	_ -> 0

data CameraAction = CameraNone | CameraRotate (V2 Float) | CameraStart | CameraStop
	deriving (Eq, Ord)

extractCameraControl :: ControlEvent -> Maybe CameraAction
extractCameraControl c = case c of
	CameraStopMovement -> Just CameraStop
	CameraStartMovement -> Just CameraStart
	CameraMotion m -> Just $ CameraRotate m
	_ -> Nothing

-- this is just a passthrough handler that shoves all its events into a TChan for something else to deal with (see: the physics function)
userInterfaceHandler :: V2 Int -> TChan ControlEvent -> EventHandler os
userInterfaceHandler windowSize pcAction = EHS
	()
	(\_ ev -> let
			relPos :: V2 Int -> V2 Float
			relPos pt = (fromIntegral <$> (pt - half)) / (fromIntegral <$> half)
				where
					half = (`div` 2) <$> windowSize
		in case ev of
			MouseDown MouseButton'2 _ -> emit $ CameraStartMovement
			MouseUp MouseButton'2 _ -> emit $ CameraStopMovement
			MouseMove px -> emit $ CameraMotion $ relPos px
			KeyDown Key'Down  -> emit $ PCStartImpel (V2   1    0 )
			KeyDown Key'Up    -> emit $ PCStartImpel (V2 (-1)   0 )
			KeyDown Key'Left  -> emit $ PCStartImpel (V2   0    1 )
			KeyDown Key'Right -> emit $ PCStartImpel (V2   0  (-1))

			KeyUp Key'Down    -> emit $ PCStopImpel  (V2   1    0 )
			KeyUp Key'Up      -> emit $ PCStopImpel  (V2 (-1)   0 )
			KeyUp Key'Left    -> emit $ PCStopImpel  (V2   0    1 )
			KeyUp Key'Right   -> emit $ PCStopImpel  (V2   0  (-1))
			_ -> nothing
	)
	(const id)
	(\ev -> liftIO . atomically $ writeTChan pcAction ev
	)
	(pure ())
	(pure ())



-- todo: allow `vals` to have multiple entries, or something like that, for when you have a very highly-sloped tile that should be textured as a 1x2 or 1x3 segment. not sure exactly how to structure that yet tho. (it would only have to be 1xn though since it's not possible for a tile to be stretched on _both_ axes, but it raises the question of how to align the textures since it might make sense to rotate them to have a 'vertical' alignment regardless of the 'natural' tile orientation & other fiddly math decisions)
data GameMap = GameMap
	{ size :: V2 Word8 -- V2 x y
	, heights :: Ptr Word8 -- x * y * 4 entries (4 corners for each tile)
	, vals :: Ptr Word16 -- x * y entries (1 sprite value for each tile)
	-- arguably this could be half-size: since if you have two edge-adjacent tiles, no matter their heights only one side will be drawn (with the exception of crosscut triangular edges, which are sufficiently unusual i think we can discard). this means that if we have them share their data slots, then we can halve the size of the allocation without actually causing any problems anywhere. that would mean slightly more involved indexing math, so it's not really worth it to start out (or ever), but if i really want to placate my data-packing urges it would be a nice place to start
	, edges :: Ptr Word16 -- x * y * 4 * 8 entries ( 4 edges for each tile + 8 sprites allowed per tile)
	}
	deriving (Eq, Ord, Show)

{-
the total size for a map of size x*y is x*y*70, with 64 of that being wall sprite data
... so a 256x256 map would be ~4.5 mb (that's roughly the size of all of BOF3's overworld maps)
-}
type TileData =
	( (Word8, Word8, Word8, Word8) -- corner heights
	, Word16 -- floor sprite
	, ([Word16], [Word16], [Word16], [Word16]) -- wall sprites, may have up to length 8 each
	)

allocateMap :: V2 Word8 -> IO GameMap
allocateMap size@(V2 x y) = do
	let edges = 4
	let wallSpriteLimit = 8
	let xy = fromIntegral x * fromIntegral y
	heights <- callocBytes (xy * edges * sizeOf (undefined :: Word8))
	sprites <- callocBytes (xy * sizeOf (undefined :: Word16))
	edges <- callocBytes (xy * edges * wallSpriteLimit * sizeOf (undefined :: Word16))
	return $ GameMap size heights sprites edges

initializeMap :: V2 Word8 -> (V2 Int -> TileData) -> IO GameMap
initializeMap size@(V2 w h) gen = do
	let edges = 4
	let wallSpriteLimit = 8
	let xy = fromIntegral w * fromIntegral h
	heights <- callocBytes (xy * edges * sizeOf (undefined :: Word8))
	sprites <- callocBytes (xy * sizeOf (undefined :: Word16))
	edges <- callocBytes (xy * edges * wallSpriteLimit * sizeOf (undefined :: Word16))
	let map = GameMap size heights sprites edges
	(\i -> let
			x = i `mod` fromIntegral w
			y = i `div` fromIntegral w
			(corners, sprite, walls) = gen $ V2 x y
		in setTileData map (V2 x y) corners sprite walls
			) `mapM_` [0..xy-1]
	return $ map

getTile :: GameMap -> V2 Int -> Maybe (IO TileData)
getTile (GameMap size@(V2 w h) heights vals edges) pt@(V2 x y) =
	if not (between 0 (fromIntegral w-1) x && between 0 (fromIntegral h-1) y)
		then Nothing
		else Just $ do
			let i = y * fromIntegral w + x
			h0 <- peekElemOff heights (i * 4 + 0)
			h1 <- peekElemOff heights (i * 4 + 1)
			h2 <- peekElemOff heights (i * 4 + 2)
			h3 <- peekElemOff heights (i * 4 + 3)
			s <- peekElemOff vals i
			w0 <- reverse <$> peekUpTo (== 0) 8 (plusPtr edges ((i * 32 +  0) * sizeOf (undefined :: Word16)))
			w1 <- reverse <$> peekUpTo (== 0) 8 (plusPtr edges ((i * 32 +  8) * sizeOf (undefined :: Word16)))
			w2 <- reverse <$> peekUpTo (== 0) 8 (plusPtr edges ((i * 32 + 16) * sizeOf (undefined :: Word16)))
			w3 <- reverse <$> peekUpTo (== 0) 8 (plusPtr edges ((i * 32 + 24) * sizeOf (undefined :: Word16)))
			return
				( (h0, h1, h2, h3)
				, s
				, (w0, w1, w2, w3)
				)

setTileData :: GameMap -> V2 Int -> (Word8, Word8, Word8, Word8) -> Word16 -> ([Word16], [Word16], [Word16], [Word16]) -> IO ()
setTileData gameMap@(GameMap (V2 w h) heights sprites walls) tPos@(V2 x y) tCorners tSprite tWalls =
	if not (between 0 (fromIntegral w-1) x && between 0 (fromIntegral h-1) y)
		then return ()
		else do
			setTileHeightCorners gameMap tPos tCorners
			setTileSprite gameMap tPos tSprite
			setTileWalls gameMap tPos tWalls

setTileHeightFlat :: GameMap -> V2 Int -> Word8 -> IO ()
setTileHeightFlat (GameMap { size = (V2 w h), heights = heights }) (V2 x y) t =
	if not (between 0 (fromIntegral w-1) x && between 0 (fromIntegral h-1) y)
		then return ()
		else do
			let i = y * fromIntegral w + x
			pokeElemOff heights (i * 4 + 0) t
			pokeElemOff heights (i * 4 + 1) t
			pokeElemOff heights (i * 4 + 2) t
			pokeElemOff heights (i * 4 + 3) t
			return ()

setTileHeightCorners :: GameMap -> V2 Int -> (Word8, Word8, Word8, Word8) -> IO ()
setTileHeightCorners (GameMap { size = V2 w h, heights = heights }) (V2 x y) (h0, h1, h2, h3) =
	if not (between 0 (fromIntegral w-1) x && between 0 (fromIntegral h-1) y)
		then return ()
		else do
			let i = y * fromIntegral w + x
			pokeElemOff heights (i * 4 + 0) h0
			pokeElemOff heights (i * 4 + 1) h1
			pokeElemOff heights (i * 4 + 2) h2
			pokeElemOff heights (i * 4 + 3) h3

setTileSprite :: GameMap -> V2 Int -> Word16 -> IO ()
setTileSprite (GameMap { size = V2 w h, vals = sprites }) (V2 x y) s =
	if not (between 0 (fromIntegral w-1) x && between 0 (fromIntegral h-1) y)
		then return ()
		else do
			let i = y * fromIntegral w + x
			pokeElemOff sprites i s

setTileWalls :: GameMap -> V2 Int -> ([Word16],[Word16],[Word16],[Word16]) -> IO ()
setTileWalls (GameMap { size = V2 w h, edges = walls }) (V2 x y) (w0_, w1_, w2_, w3_) =
	if not (between 0 (fromIntegral w-1) x && between 0 (fromIntegral h-1) y)
		then return ()
		else let
				i = y * fromIntegral w + x
				w0 = take 8 w0_
				w1 = take 8 w1_
				w2 = take 8 w2_
				w3 = take 8 w3_
			in do
				pokeUpTo 8 (plusPtr walls ((i * 32 +  0) * sizeOf (undefined :: Word16))) w0
				pokeUpTo 8 (plusPtr walls ((i * 32 +  8) * sizeOf (undefined :: Word16))) w1
				pokeUpTo 8 (plusPtr walls ((i * 32 + 16) * sizeOf (undefined :: Word16))) w2
				pokeUpTo 8 (plusPtr walls ((i * 32 + 24) * sizeOf (undefined :: Word16))) w3

setTileWall :: GameMap -> V2 Int -> Direction -> [Word16] -> IO ()
setTileWall (GameMap { size = V2 w h, edges = walls }) (V2 x y) dir ws_ =
	if not (between 0 (fromIntegral w-1) x && between 0 (fromIntegral h-1) y)
		then return ()
		else let
				i = y * fromIntegral w + x
				ws = take 8 ws_
			in case dir of
				North -> pokeUpTo 8 (plusPtr walls ((i * 32 +  0) * sizeOf (undefined :: Word16))) ws
				East  -> pokeUpTo 8 (plusPtr walls ((i * 32 +  8) * sizeOf (undefined :: Word16))) ws
				South -> pokeUpTo 8 (plusPtr walls ((i * 32 + 16) * sizeOf (undefined :: Word16))) ws
				West  -> pokeUpTo 8 (plusPtr walls ((i * 32 + 24) * sizeOf (undefined :: Word16))) ws

-- this would be a good place to bring in `Polyhedra` models but that's maybe too much for this tiny program
pcModel :: GLFWContext os (Buffer os BasicVertex)
pcModel = do
	buffer <- newBuffer $ 6 * 6 + 1 -- 6 faces w/ 6 vertices each
	let white = V3 1 1 1
	(\dir -> let
			norm = dirNorm dir
			(base, over) = case dir of
				North -> (V3 (-8) 0 (-8), V3   16  0    0 )
				East  -> (V3   8  0 (-8), V3    0  0   16 )
				South -> (V3   8  0   8 , V3 (-16) 0    0 )
				West  -> (V3 (-8) 0   8 , V3    0  0 (-16))
			up = V3 0 (2 * 16) 0
			v0 = (base + up + over, white, norm, uv 221 0)
			v1 = (base + up       , white, norm, uv 221 1)
			v2 = (base            , white, norm, uv 237 2)
			v3 = (base      + over, white, norm, uv 237 3)
		in GP.writeBuffer buffer (fromEnum dir * 6)
				[v0, v2, v1
				,v0, v3, v2
				]
		) `mapM_` [North, East, South, West]
	-- bottom face
	GP.writeBuffer buffer (4 * 6)
		$ (\(pos, norm, uv) -> (pos - V3 8 0 8, white, norm * V3 1 (-1) 1, uv)) <$> reverse (quad 205)
	-- top face
	GP.writeBuffer buffer (5 * 6)
		$ (\(pos, norm, uv) -> (pos - V3 8 0 8 + V3 0 (2 * 16) 0, white, norm, uv)) <$> quad 253
	return $ Buffer buffer (6 * 6 + 1)


renderMap :: GameMap -> GLFWContext os (Buffer os BasicVertex)
renderMap (GameMap (V2 w h) heights sprites walls) = do

	let wh = fromIntegral w * fromIntegral h
	let tileSize = wh * 6 -- 6 vertices per quad
	let wallSize = wh * 4 * 8 * 6 -- 4 sides, with up to 8 sprites each, with 6 vertices per quad
	buffer <- newBuffer $ tileSize + wallSize

	(\i -> do
		c0 <- liftIO $ peekElemOff heights (i * 4 + 0)
		c1 <- liftIO $ peekElemOff heights (i * 4 + 1)
		c2 <- liftIO $ peekElemOff heights (i * 4 + 2)
		c3 <- liftIO $ peekElemOff heights (i * 4 + 3)
		s <- liftIO $ peekElemOff sprites i
		let xy = V2 (i `mod` fromIntegral w) (i `div` fromIntegral w)
		let verts = quadGen xy (c0, c1, c2, c3) s
		GP.writeBuffer buffer (i * 6) verts
		) `mapM_` [0..wh-1]
	(\i -> do
		let xy@(V2 x y) = V2 (i `mod` fromIntegral w) (i `div` fromIntegral w)
		let bounds = Rect (V2 0 0) (fromIntegral <$> V2 (w-1) (h-1))
		(\dir -> do
			let (wallOffset, (eOff0, eOff1), (aOff0, aOff1), adjXY@(V2 ax ay)) = case dir of
				North -> ( 0, (0, 1), (3, 2), V2  x   (y-1))
				East  -> ( 8, (1, 2), (0, 3), V2 (x+1) y   )
				South -> (16, (2, 3), (1, 0), V2  x   (y+1))
				West  -> (24, (3, 0), (2, 1), V2 (x-1) y   )
			walls <- liftIO $ peekUpTo (== 0) 8 $ plusPtr walls ((i * 32 + wallOffset) * sizeOf (undefined :: Word16))
			e0 <- liftIO $ peekElemOff heights (i * 4 + eOff0)
			e1 <- liftIO $ peekElemOff heights (i * 4 + eOff1)
			let j = ay * fromIntegral w + ax
			ae0 <- if inRect bounds adjXY
				then liftIO $ peekElemOff heights (j * 4 + aOff0)
				else pure e0
			ae1 <- if inRect bounds adjXY
				then liftIO $ peekElemOff heights (j * 4 + aOff1)
				else pure e1
			let verts = wallGen xy dir (e0, e1) (ae0, ae1) walls
			-- todo: figure out the correct index to write to. idk if it's this
			GP.writeBuffer buffer (tileSize + (i * 4 * 8 + wallOffset) * 6) verts
			) `mapM_` [North, East, South, West]
		) `mapM_` [0..wh-1]

	return $ Buffer buffer $ tileSize + wallSize

quadGen :: V2 Int -> (Word8, Word8, Word8, Word8) -> Word16 -> [(V3 Float, V3 Float, V3 Float, V2 Float)]
quadGen xy (c0_, c1_, c2_, c3_) ix_ =
		-- if you want this to be _really_ complex, automatically support non-planar corner heights by changing the vertex ordering to slice diagonally both ways
		[ v0, v2, v1
		, v0, v3, v2
		]
	where
		to3d (V2 x z) = V3 x 0 z
		xyf = to3d $ (fromIntegral <$> xy) ^* 16
		c0 = fromIntegral c0_ * 16
		c1 = fromIntegral c1_ * 16
		c2 = fromIntegral c2_ * 16
		c3 = fromIntegral c3_ * 16
		ix = if ix_ == 0
			then 255
			else fromIntegral ix_
		white = V3 1 1 1
		norm = V3 0 1 0 -- TODO: calc norm from corner heights
		v0 = (xyf + V3  0 c0  0, white, norm, uv ix 0)
		v1 = (xyf + V3 16 c1  0, white, norm, uv ix 1)
		v2 = (xyf + V3 16 c2 16, white, norm, uv ix 2)
		v3 = (xyf + V3  0 c3 16, white, norm, uv ix 3)

-- this is a stub function that just generates a single quad
wallGen :: V2 Int -> Direction -> (Word8, Word8) -> (Word8, Word8) -> [Word16] -> [(V3 Float, V3 Float, V3 Float, V2 Float)]
wallGen xy dir (e0_, e1_) (a0_, a1_) sprites = if e0_ <= a0_ && e1_ <= a1_
	then [] -- the tiles are flush and don't need any joiner (or the joiner will be handled by the other tile)
	else
		[ v0, v2, v1
		, v0, v3, v2
		]
	where
		to3d (V2 x z) = V3 x 0 z
		xyf = to3d $ (fromIntegral <$> xy) ^* 16
		e0 = fromIntegral e0_ * 16
		e1 = fromIntegral e1_ * 16
		a0 = fromIntegral a0_ * 16
		a1 = fromIntegral a1_ * 16
		(base, over) = case dir of
			North -> (V3  0 0  0, V3   16  0    0 )
			East  -> (V3 16 0  0, V3    0  0   16 )
			South -> (V3 16 0 16, V3 (-16) 0    0 )
			West  -> (V3  0 0 16, V3    0  0 (-16))
		white = V3 1 1 1
		norm = dirNorm dir
		-- todo: assign UVs based on world coord (which would also mean the height value couldn't be Word8 any more since we'd want to allow half-steps.)
		v0 = (xyf + base + over + V3 0 e1 0, white, norm, uv 255 0)
		v1 = (xyf + base +        V3 0 e0 0, white, norm, uv 255 1)
		v2 = (xyf + base +        V3 0 a0 0, white, norm, uv 255 2)
		v3 = (xyf + base + over + V3 0 a1 0, white, norm, uv 255 3)

-- idk if these are right
dirNorm :: Direction -> V3 Float
dirNorm dir = case dir of
	North -> V3   0  0   1
	East  -> V3 (-1) 0   0
	South -> V3   0  0 (-1)
	West  -> V3   1  0   0

heightOnTile :: (Word8, Word8, Word8, Word8) -> V2 Float -> Float
heightOnTile (nw, ne, se, sw) (V2 x y) = linear east west x
	where
		west = linear (fromIntegral sw) (fromIntegral nw) y
		east = linear (fromIntegral se) (fromIntegral ne) y

{-
tilesAlongLine :: V2 Float -> V2 Float -> Float -> [V2 Int]
tilesAlongLine start stop r = []
	where
		tileUnderPt :: V2 Float -> V2 Int
		tileUnderPt (V2 x y) = V2 (floor $ (x `mod''` 16) / 16) (floor $ (z `mod''` 16) / 16)
-}

linear :: Num a => a -> a -> a -> a
linear a b p = a * p + b * (1 - p)



pokeUpTo :: Storable a => Int -> Ptr a -> [a] -> IO ()
pokeUpTo _ _   [] = pure ()
pokeUpTo l ptr (v:vs) = case l of
	0 -> pure ()
	_ -> do
		pokeElemOff ptr 0 v
		pokeUpTo (l-1) (plusPtr ptr $ sizeOf v) (vs)

peekUpTo :: Storable a => (a -> Bool) -> Int -> Ptr a -> IO [a]
peekUpTo stop l ptr = reverse <$> go l ptr
	where
		go l ptr = case l of
			0 -> pure []
			_ -> do
				x <- peekElemOff ptr 0
				if stop x
					then pure []
					else (x :) <$> go (l-1) (plusPtr ptr $ sizeOf x)


asBuffer :: V3 Float -> [(V3 Float, V3 Float, V2 Float)] -> GLFWContext os (Buffer os BasicVertex)
asBuffer color verts = do
	let l = length verts + 1
	let realVerts = (\(pos, norm, uv) -> (pos, color, norm, uv)) <$> verts

	setupBuffer l realVerts

{- generating 3d data:

these assume a 'grid unit' will be a 16x16x16 cube
currently they force heights to be an even grid unit; this should maybe be changed to be a fractional value
-}

quad :: Int -> [(V3 Float, V3 Float, V2 Float)]
quad ix =
		[ v0, v2, v1
		, v0, v3, v2
		]
	where
		v0 = (V3  0 0  0, V3 0 1 0, uv ix 0)
		v1 = (V3 16 0  0, V3 0 1 0, uv ix 1)
		v2 = (V3 16 0 16, V3 0 1 0, uv ix 2)
		v3 = (V3  0 0 16, V3 0 1 0, uv ix 3)

data Direction = North | East | South | West
	deriving (Eq, Ord, Enum, Bounded, Show, Read)

-- this assumes a 16x16 sprite grid (in terms of sprites-per-row/column, not of pixel size) which isn't really tenable for a Word16 sprite value
uv :: Int -> Int -> V2 Float
uv ix corner = case corner `mod` 4 of
		0 -> V2  xf     yf
		1 -> V2 (xf+o)  yf
		2 -> V2 (xf+o) (yf+o)
		3 -> V2  xf    (yf+o)
		_ -> error "unreachable"
	where
		x = ix `mod` 16
		y = ix `div` 16
		xf = fromIntegral x / 16
		yf = fromIntegral y / 16
		o = 1 / 16


data GameState
	= GameState
		{ camera :: Camera Float
		, worldGeometry :: Int
		, pcImpulse :: V2 Float -- xz movement coords
		, cameraRotation :: Maybe (V2 Float) -- yaw and pitch angles
		, pcState :: ActorState
		, loadedMap :: GameMap
		}
	deriving (Eq, Ord, Show)

data ActorState = ActorState
	{ location :: V3 Float
	, velocity :: V3 Float -- derivative of position
	, acceleration :: V3 Float -- derivative of velocity

	, yaw :: Float
	, movement :: Movement
	}
	deriving (Eq, Ord, Show, Read)
data Movement = Standing | Falling
	deriving (Eq, Ord, Show, Read)

-- this is assuming something else is determining the acceleration
evaluate :: ActorState -> Float -> ActorState
evaluate (ActorState pos vel acc yaw move) dt = ActorState pos' vel' acc yaw move
	where
		vel' =           acc  ^* dt
		pos' =           vel' ^* dt
		-- i'm _assuming_ this would damp all acceleration
		acc' = acc + ((acc * V3 (-0.95)) ^* dt)

