{-# LANGUAGE PackageImports #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE DuplicateRecordFields #-}

import Prelude hiding ((.), id)

import "GPipe-GLFW4" Graphics.GPipe.Context.GLFW (ModifierKeys(..), MouseButtonState(..), MouseButton(..), KeyState(..), Key(..))
import qualified "GPipe-GLFW4" Graphics.GPipe.Context.GLFW as GLFW
import Graphics.GPipe hiding (FlatVFloat(..), (^*), distance, Buffer, writeBuffer)
import qualified Graphics.GPipe as GP (Format(..), Buffer, writeBuffer)
import Codec.Picture

import Control.Category
import Control.Concurrent.STM (STM, atomically)
import Control.Concurrent.STM.TVar
import Control.Concurrent.STM.TChan
import Control.Monad.IO.Class
import Control.Monad.Exception

import Control.Monad
import Control.Monad.Writer
import Data.Bits
import Data.Char (ord)
import Data.Word
import Data.Vector.Storable (Vector, (!))
import qualified Data.Vector.Storable as V
import Data.Maybe
import Data.List
import Data.Foldable
import Data.Functor.Contravariant
import Foreign.Storable

import Linear

import Types
import qualified Game (setupMainLoopCallbacks)

main :: IO ()
main = runContextT GLFW.defaultHandleConfig $ do
	let startingWindowSize
	-- = V2 1024 768 -- 3:4 aspect ratio
		= V2 1080 675 -- 16:10 aspect ratio

	let wininfo = WinInfo "TEST GAME" startingWindowSize
	window <- makeContext wininfo

	-- frp setup
	executionState <- liftIO $ newTVarIO (Execution False False [] startingWindowSize)
	pulse <- setRawCallbacks window

	( startingHandler
	 , physicsCallback
	 , renderCallback ) <- Game.setupMainLoopCallbacks window executionState

	pushHandler executionState startingHandler

	-- start looping
	let loop = do
		-- run events, then poll event network to see if we should quit
		(windowState, shouldQuit) <- do
			windowState <- executeHandler pulse executionState
			shouldQuit <- liftIO $ getQuit executionState
			return (windowState, shouldQuit)

		-- problem: when we run through a bunch of events we'll only collect the elapsed time since the _last_ event processed, not from the last time check. this means time moves 'slower' when a bunch of events are happening
		let e = realToFrac $ elapsed windowState
		-- ... render geometry
		renderCallback e

		-- ... run physics
		physicsCallback e

		if shouldQuit
			then return ()
			else loop
	loop
	return ()

data WindowInfo = WinInfo
	{ windowTitle :: String
	, windowSize :: V2 Int
	}
	deriving (Eq, Ord, Show, Read)



makeContext :: WindowInfo -> GLFWContext os (Window os RGBAFloat Depth)
makeContext wininfo =
	newWindow (WindowFormatColorDepth GP.SRGB8A8 GP.Depth16)
		$ (GLFW.defaultWindowConfig (windowTitle wininfo))
			{ GLFW.configWidth = width
			, GLFW.configHeight = height
			}
	where
		V2 width height = windowSize wininfo



-- arguably this should store mousePosition as a Maybe & have `CursorLeaveEvent`s set it to Nothing
data InputState = InputState
	{ cursorPosition :: V2 Int
	-- if the given button is currently down, where the pointer was when the button was first pressed
	, dragStart :: [(MouseButton, V2 Int)]
	, heldKeys :: [Key]
	, thisEvent :: Double
	, lastEvent :: Double
	, elapsed :: Double
	}
	deriving (Eq, Ord, Show, Read)

type Pulse a = IO (InputState, [a])

-- todo: it would be nice if this automatically used GLFW.setTime to reset the timer when it started to lose precision, while still outputting reasonable `elapsed` values
setRawCallbacks :: Window os c ds -> GLFWContext os (Pulse GLFWEvent)
setRawCallbacks win = do
	eventChannel <- do
		chan <- liftIO $ newTChanIO
		let fire = atomically . writeTChan chan

		GLFW.setKeyCallback win $ Just $ \key int state mods ->
			fire $ KeyEvent key int state mods
		GLFW.setCharCallback win $ Just $ \char ->
			fire $ CharEvent char

		GLFW.setCursorPosCallback win $ Just $ \x y ->
			fire $ CursorPosEvent x y
		GLFW.setCursorEnterCallback win $ Just $ \state -> case state of
			GLFW.CursorState'InWindow -> fire $ CursorEnterEvent
			GLFW.CursorState'NotInWindow -> fire $ CursorLeaveEvent


		GLFW.setMouseButtonCallback win $ Just $ \button state mod ->
			fire $ MouseButtonEvent button state mod
		GLFW.setScrollCallback win $ Just $ \x y -> -- ??
			fire $ ScrollEvent x y

		GLFW.setWindowCloseCallback win $ Just $
			fire $ WindowCloseEvent

		return chan


	windowStateVar <- liftIO $ newTVarIO $ InputState (V2 0 0) [] [] 0 0 0
	let pulse = liftIO $ do
			windowState <- readTVarIO windowStateVar
			(newState, evs) <- getAll windowState []
			atomically $ writeTVar windowStateVar $ newState
			return $ (newState, reverse evs)
		where
			-- every time we run `pulse` we want to update the time _once_, so that the elapsed calculation is correct. (if we run it multiple times then we'll only get the time elapsed from the last two processed events.) so `getAll` processes all the events in one go, and only then updates the time once
			getAll :: InputState -> [GLFWEvent] -> IO (InputState, [GLFWEvent])
			getAll oldState oldEvs = do
				evs <- atomically $ collectTChan eventChannel
				let (newState, mnewEvs) = mapAccumL (\state ev -> let
							updateState = foldr (.) id $ catMaybes [updateKeys ev, updatePos ev] :: InputState -> InputState
							newState = updateState oldState :: InputState
						in
							(newState, convertGLFW oldState ev)
					) oldState evs
				now <- GLFW.getTime
				return
					-- this is kind of goofy since `updateTime` will only return Nothing if `now` is Nothing (due to the glfw context not existing)
					( case updateTime now of
						Nothing -> newState
						Just f -> f newState
					, catMaybes mnewEvs
					)

	return pulse

collectTChan :: TChan a -> STM [a]
collectTChan chan =
	let go = do
		ma <- tryReadTChan chan
		case ma of
			Nothing -> pure []
			Just a -> (a :) <$> go
	in reverse <$> go

executeHandler :: Pulse GLFWEvent -> TVar (ExecutionState os) -> GLFWContext os InputState
executeHandler getEvs handlerT = do
	(windowState, evs) <- liftIO $ getEvs
	handler <- liftIO $ getHandler handlerT
	handler' <- case handler of
		EHS z gen update exec act deact -> do
			z' <- ((\s ev -> case execWriter $ gen s ev of
					[] -> pure s
					as -> foldlM (\s a -> exec a >> pure (update a s)) s as
				) `foldlM` z) evs
			-- gotta remake the whole value b/c of the quantified existential
			return $ EHS z' gen update exec act deact
	-- handlerChanged will be set true if the above event processing has triggered a handler change. in that case, the existing handler should be discarded, otherwise the updated handler should be stored in the execution state
	liftIO $ atomically $ modifyTVar handlerT $ \h -> if handlerChanged h
			then h { handlerChanged = False }
			else h { handlerChanged = False, handlers = handler' : tail (handlers h) }
	return windowState





updateTime :: Maybe Double -> Maybe (InputState -> InputState) -- , or Double -> InputState -> InputState mapped over a Maybe Double
updateTime me = (\e s -> s
	{ thisEvent = e
	, lastEvent = thisEvent s
	, elapsed = e - thisEvent s
	}) <$> me
updatePos :: RawGLFWEvent -> Maybe (InputState -> InputState)
updatePos ev = case ev of
	CursorPosEvent x y -> Just $ \s -> s { cursorPosition = floor <$> V2 x y }
	MouseButtonEvent button state _ -> case state of
		MouseButtonState'Pressed -> Just $ \s -> s { dragStart = (button, cursorPosition s) : dragStart s }
		MouseButtonState'Released -> Just $ \s -> s { dragStart = filter ((/= button) . fst) $ dragStart s }
	_ -> Nothing
updateKeys :: RawGLFWEvent -> Maybe (InputState -> InputState)
updateKeys ev = case ev of
	KeyEvent key _ press _ -> case press of
		KeyState'Pressed -> Just $ \s -> s { heldKeys = key : heldKeys s }
		KeyState'Released -> Just $ \s -> s { heldKeys = filter (== key) $ heldKeys s }
		_ -> Nothing
	_ -> Nothing

-- TODO: it'd be nice to expose double-clicks (store when the last mousedown was, & send a different event if the timing is under a given window? that might require merging some of the above functions so they can see the current time AND current event at the same time)
convertGLFW :: InputState -> RawGLFWEvent -> Maybe GLFWEvent
convertGLFW is ev = case ev of
	MouseButtonEvent button state _ -> case state of
		MouseButtonState'Pressed -> Just $ MouseDown button (cursorPosition is)
		MouseButtonState'Released -> Just $ MouseUp button (cursorPosition is)
	KeyEvent k _ state _ -> case state of
		KeyState'Pressed -> Just $ KeyDown k
		KeyState'Released -> Just $ KeyUp k
		-- these fire too fast and i don't thing there's any way to tell glfw to increase the repeat interval, so this might be something worth doing manually w/ timecodes
		KeyState'Repeating -> Just $ KeyRepeat k
	CharEvent c -> Just $ Typed c
	CursorPosEvent x y -> Just $ MouseMove $ round <$> V2 x y
	WindowCloseEvent -> Just WindowClose
	_ -> Nothing
